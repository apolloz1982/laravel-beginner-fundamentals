<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use Illuminate\Http\Request;

class PostTagController extends Controller
{
    public function index($tag)
    {
        $tag = Tag::findOrFail($tag);
        // return view('posts.index', [
        //     'posts' => $tag->blogPosts, 
        //     'mostCommented' => [],
        //     'mostActive' => [],
        //     'mostActiveLastMonth' => [],
        // ]);

        // return view('posts.index', [
        //     'posts' => $tag->blogPosts()
        //     ->withCount('comments')
        //     ->with('user')
        //     ->with('tags')
        //     ->get()
        // ]);

        return view('posts.index', [
            'posts' => $tag->blogPosts()
                ->latestWithRelations()
                ->get()
        ]);

    }
}
