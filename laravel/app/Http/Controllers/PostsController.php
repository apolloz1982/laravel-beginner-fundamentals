<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePost;
use App\Models\BlogPost;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;
use App\Models\Image;

class PostsController extends Controller
{
    
    public function __construct()
    {
        // $this->middleware('auth');
        $this->middleware('auth')
            ->only(['create', 'store', 'edit', 'update', 'destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // DB::enableQueryLog();

        // ###  Lazy loading
        // $posts = BlogPost::all(); 
        // foreach($posts as $post) 
        // {  
        //     foreach($post->comments as $comment)
        //     {
               
        //     }
        // }
        // $queries = DB::getQueryLog();
        // dd($queries);

        // ### Eager Loading
        // $posts = BlogPost::with('comments')->get();
        // foreach($posts as $post) 
        // {  
        //     foreach($post->comments as $comment)
        //     {
               
        //     }
        // }
        // $queries = DB::getQueryLog();
        // dd($queries);

        // ###  Lazy & Eager loading
        // $posts = BlogPost::all(); 
        // $posts->load('comments');
        // foreach($posts as $post) 
        // {  
        //     foreach($post->comments as $comment)
        //     {
               
        //     }
        // }
        // $queries = DB::getQueryLog();
        // dd($queries);

        // Using without sGlobal
        // return view('posts.index', ['posts' => BlogPost::withCount('comments')
        //     ->orderBy('created_at', 'desc')
        //     ->get()]);

        // Order using Using Global
        // return view('posts.index', 
        //     ['posts' => BlogPost::withCount('comments')->get()]);

        // Order using Using local
        // return view('posts.index', 
        //     [
        //         // 'posts' => BlogPost::latest()->withCount('comments')->get(), //Fix Queries 1xx times with -> with('user')  
        //         'posts' => BlogPost::latest()->withCount('comments')->with('user')->get(), // Queries 6 times
        //         'mostCommented' => BlogPost::mostCommented()->take(5)->get(),
        //         'mostActive' => User::withMostBlogPosts()->take(5)->get(),
        //         'mostActiveLastMonth' => User::withMostBlogPostsLastMonth()->take(5)->get(),

        //     ]
        // );

        // return view('posts.index', ['posts' => BlogPost::all()]);
        // return view('posts.index', ['posts' => BlogPost::orderBy('created_at', 'desc')->take(5)->get()]);

        //Cache
        // $mostCommented = Cache::remember('mostCommented', now()->addSecond(60), function(){
        //     return BlogPost::mostCommented()->take(5)->get();
        // });
        // $mostActive = Cache::remember('mostActive', now()->addSecond(60), function(){
        //     return User::withMostBlogPosts()->take(5)->get();
        // });
        // $mostActiveLastMonth = Cache::remember('mostActiveLastMonth', now()->addSecond(60), function(){
        //     return User::withMostBlogPostsLastMonth()->take(5)->get();
        // });

        //Cache Tags
        // $mostCommented =  Cache::tags('blog-post')->remember('mostCommented', now()->addSecond(60), function(){
        //     return BlogPost::mostCommented()->take(5)->get();
        // });
        // $mostActive =  Cache::tags('blog-post')->remember('mostActive', now()->addSecond(60), function(){
        //     return User::withMostBlogPosts()->take(5)->get();
        // });
        // $mostActiveLastMonth =   Cache::tags('blog-post')->remember('mostActiveLastMonth', now()->addSecond(60), function(){
        //     return User::withMostBlogPostsLastMonth()->take(5)->get();
        // });

        // return view('posts.index', 
        //     [
        //         'posts' => BlogPost::latest()->withCount('comments')
        //             ->with('user')
        //             ->with('tags')
        //             ->get(), 
        //         'mostCommented' => $mostCommented,
        //         'mostActive' => $mostActive,
        //         'mostActiveLastMonth' => $mostActiveLastMonth,

        //     ]
        // );

        // return view('posts.index', 
        // [
        //     'posts' => BlogPost::latest()->withCount('comments')
        //         ->with('user')
        //         ->with('tags')
        //         ->get()
        // ]
        return view('posts.index', 
        [
            'posts' => BlogPost::latestWithRelations()->get()
        ]
    );

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $this->authorize('posts.create');
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePost $request)
    {
        // With parametor Requeust
        // $post = new BlogPost();
        // $post->title = $request->iput('title');
        // $post->content = $request->iput('content');
        // $post->save();

        // $validated = $request->validated();
        // $post = new BlogPost();
        // $post->title = $validated['title'];
        // $post->content = $validated['content'];
        // $post->save();
       

        $validatedData = $request->validated(); 
        $validatedData['user_id'] = $request->user()->id;
        $blogPost = BlogPost::create($validatedData);

        if ($request->hasFile('thumbnail')) {
            $path = $request->file('thumbnail')->store('thumbnails');
            $blogPost->image()->save(
                Image::create(['path' => $path])
            );
        }

        $request->session()->flash('status', 'The blog post was created!');

        return redirect()->route('posts.show', ['post' => $blogPost->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // abort_if(!isset($this->posts[$id]), 404);
        // return view('posts.show', ['post' => BlogPost::findOrFail($id)]);
        // return view('posts.show', ['post' => BlogPost::with('comments')->findOrFail($id)]);

        // scope query locat solution #1
        // return view('posts.show', [
        //     'post' => BlogPost::with(['comments' => function($query){
        //         return $query->latest();
        //     }])->findOrFail($id)
        // ]);

        // scope query locat solution #2 , add latest() in Medel class -> return $this->belongsTo('App\Models\BlogPost')->latest();
        // return view('posts.show', ['post' => BlogPost::with('comments')->findOrFail($id)]);
        //Cache
        // $blogPost = Cache::remember("blog-post-{$id}", now()->addSecond(60), function() use($id) {
        //     return BlogPost::with('comments')->findOrFail($id);
        // });

         //Cache Tags
         $blogPost = Cache::tags('blog-post')->remember("blog-post-{$id}", now()->addSecond(60), function() use($id) {
            //Fix with Eager load 
            // return BlogPost::with('comments')
            //     ->with('tags')
            //     ->with('user')
            //     ->findOrFail($id);

            // return BlogPost::with('comments')
            // ->with('tags')
            // ->with('user')
            // ->with('comments.user')
            // ->findOrFail($id);     Or
            return BlogPost::with('comments', 'tags', 'user', 'comments.user')->findOrFail($id);
        });

        $sessionId = session()->getId();
        $counterKey = "blog-post-{$id}-counter";
        $usersKey = "blog-post-{$id}-users";

        $users = Cache::tags('blog-post')->get($usersKey, []);
        $usersUpdate = [];
        $diffrence = 0;
        $now = now();

        foreach ($users as $session => $lastVisit) {
            if ($now->diffInMinutes($lastVisit) >= 1) {
                $diffrence--;
            } else {
                $usersUpdate[$session] = $lastVisit;
            }
        }

        if(
            !array_key_exists($sessionId, $users)
            || $now->diffInMinutes($users[$sessionId]) >= 1
        ) {
            $diffrence++;
        }

        $usersUpdate[$sessionId] = $now;
        Cache::tags('blog-post')->forever($usersKey, $usersUpdate);

        if (!Cache::tags('blog-post')->has($counterKey)) {
            Cache::tags('blog-post')->forever($counterKey, 1);
        } else {
            Cache::tags('blog-post')->increment($counterKey, $diffrence);
        }
        
        $counter = Cache::tags('blog-post')->get($counterKey);

        return view('posts.show', [
            'post' => $blogPost,
            'counter' => $counter
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = BlogPost::findOrFail($id);

        // if(Gate::denies('update-post', $post)){
        //     abort(403, "Yout can't edit this blog post!");
        // }
        // $this->authorize( $post); or
        $this->authorize('posts.update', $post);
        return view('posts.edit', ['post' => $post]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StorePost $request, $id)
    {
        $post = BlogPost::findOrFail($id);

        // if(Gate::denies('update-post', $post)){
        //     abort(403, "Yout can't edit this blog post!");
        // }
        $this->authorize('posts.update', $post);

        $validated = $request->validated();
        $post->fill($validated);
        
        if ($request->hasFile('thumbnail')) {
            $path = $request->file('thumbnail')->store('thumbnails');

            if ($post->image) {
                Storage::delete($post->image->path);
                $post->image->path = $path;
                $post->image->save();
            } else {
                $post->image()->save(
                    Image::create(['path' => $path])
                );
            }
        }
        $post->save();

        $request->session()->flash('status', 'Blog post was updated!');

        return redirect()->route('posts.show', ['post' => $post->id]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = BlogPost::findOrFail($id);
        // if(Gate::denies('delete-post', $post)){
        //     abort(403, "Yout can't delete this blog post!");
        // }
        $this->authorize('posts.delete', $post);

        $post->delete();

        session()->flash('status', 'Blog post was deleted!');

        return redirect()->route('posts.index');
    }
}
