<?php

namespace App\View\Components;

use Illuminate\View\Component;
use Carbon\Carbon;

class Updated extends Component
{
    /**
     * The date.
     *
     * @var date
     */
    public $date;

    /**
     * The name
     *
     * @var string
     */
    public $name;

    /**
     * The userId
     *
     * @var int
     */
    public $userId;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct( $date=null,  $name=null, $userId=null)
    {
        $this->date = $date;
        $this->name = $name;
        $this->userId = $userId;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.updated');
    }
}
