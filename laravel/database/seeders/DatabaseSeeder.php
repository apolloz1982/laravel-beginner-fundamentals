<?php

namespace Database\Seeders;

use App\Models\BlogPost;
use App\Models\Comment;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use App\Models\User;
use Illuminate\Support\Facades\Cache;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        // DB::table('users')->insert([
        //     'name' => 'apolloz',
        //     'email' => 'apolloz1982@gmail.com',
        //     'email_verified_at' => now(),
        //     'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        //     'remember_token' => Str::random(10),
        // ]);

        // $admin = User::factory()->newAdmin()->create();
        // $else = User::factory()->count(20)->create();

        // $this->call(UsersTableSeeder::class);

        // $users = $else->concat([$admin]);

        // dd(get_class($d1), get_class($d2)); // Check return class

        // dd($users->count()); //21
        // $posts = BlogPost::factory()->count(50)->create(); // If user_id (foreign ) not null will be error
        
        // $this->call(BlogPostsTableSeeder::class);

        // $comments = Comment::factory()->count(150)->make()->each(function($comment) use($posts){
        //     $comment->blog_post_id = $posts->random()->id;
        //     $comment->save();
        // });

        // $this->call(CommentsTableSeeder::class);

        if($this->command->confirm('Do you want refresh the database?'))
        {
            $this->command->call('migrate:refresh');
            $this->command->info('Database was refreshed');
        }

        Cache::tags(['blog-post'])->flush();

        $this->call([
            UsersTableSeeder::class, 
            BlogPostsTableSeeder::class, 
            CommentsTableSeeder::class,
            TagsTableSeeder::class,
            BlogPostTagTableSeeder::class
        ]);
    }
}
