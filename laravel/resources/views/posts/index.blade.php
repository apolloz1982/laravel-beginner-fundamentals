@extends('layouts.app')

@section('title', 'Blog Posts')

@section('content')
    <div class="row">
        {{-- @each('posts.partials.post', $posts, 'post') --}}
        <div class="col-8">
            @forelse ($posts as $key => $post)
                @include('posts.partials.post')
            @empty
                No post found!
            @endforelse
        </div>
        <div class="col-4">
            @include('posts._activity')

            {{-- <div class="container"> --}}
                {{-- <div class="row">
                    <div class="card" style="width: 100%;">
                        <div class="card-body">
                            <h5 class="card-title">Most Commented</h5>
                            <h6 class="card-subtitle mb-2 text-muted">
                                What people are currently talking about
                            </h6>
                        </div>
                        <ul class="list-group list-group-flush">
                            @foreach ($mostCommented as $post)
                                <li class="list-group-item">
                                    <a href="{{ route('posts.show', ['post' => $post->id]) }}">
                                        {{ $post->title }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div> --}}

                {{-- <div class="row">
                    <x-card title="Most Commented">
                        @slot('subtitle')
                            What people are currently talking about
                        @endslot
                        @slot('items')
                            @foreach ($mostCommented as $post)
                                <li class="list-group-item">
                                    <a href="{{ route('posts.show', ['post' => $post->id]) }}">
                                        {{ $post->title }}
                                    </a>
                                </li>
                            @endforeach
                        @endslot
                    </x-card>
                </div> --}}

                {{-- <div class="row mt-4">
                    <div class="card" style="width: 100%;">
                        <div class="card-body">
                            <h5 class="card-title">Most Active</h5>
                            <h6 class="card-subtitle mb-2 text-muted">
                                User with most posts writen
                            </h6>
                        </div>
                        <ul class="list-group list-group-flush">
                            @foreach ($mostActive as $user)
                                <li class="list-group-item">
                                    {{ $user->name }}
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div> --}}
                {{-- <div class="row mt-4">
                    <x-card title="Most Active">
                        @slot('subtitle')
                            User with most posts writen
                        @endslot
                        @slot('items', collect($mostActive->pluck('name')))
                    </x-card>
                </div> --}}

                {{-- <div class="row mt-4">
                    <div class="card" style="width: 100%;">
                        <div class="card-body">
                            <h5 class="card-title">Most Active Last Month</h5>
                            <h6 class="card-subtitle mb-2 text-muted">
                                User with most posts writen in the last month
                            </h6>
                        </div>
                        <ul class="list-group list-group-flush">
                            @foreach ($mostActiveLastMonth as $user)
                                <li class="list-group-item">
                                    {{ $user->name }}
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div> --}}
                {{-- <div class="row mt-4">
                    <x-card title="Most Active Last Month">
                        @slot('subtitle')
                            User with most posts writen in the last month
                        @endslot
                        @slot('items', collect($mostActiveLastMonth->pluck('name')))
                    </x-card>
                </div> --}}
            {{-- </div> --}}
        </div>
    </div>
@endsection
