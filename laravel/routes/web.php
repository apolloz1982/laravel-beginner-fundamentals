<?php

use App\Http\Controllers\AboutController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PostCommentController;
use App\Http\Controllers\PostsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home.index', []);
// })->name('home.index');

// Route::get('/contact', function () {
//     return view('home.contact');
// })->name('home.contact');


Route::get('/', [HomeController::class, 'home'])
    ->name('home.index')
    ->middleware('auth');

Route::get('/contact', [HomeController::class, 'contact'])
    ->name('home.contact');
    
Route::get('/secret', 'App\Http\Controllers\HomeController@secret')
    ->name('secret')
    ->middleware('can:home.secret');

Route::get('/single', AboutController::class);

Route::get('/posts/tag/{id}', 'App\Http\Controllers\PostTagController@index')
    ->name('post.tags.index');


// Route::resource('posts', PostsController::class)
Route::resource('posts', PostsController::class)
    ->only(['index', 'show', 'create', 'store', 'edit', 'update', 'destroy']);
// Route::resource('posts', PostsController::class)->except(['index', 'show']);

// Route::get('posts', function() use($posts) {
//     // dd(request()->all());
//     // dd((int)request()->input('page', 1));

//     // https://laravel.com/docs/8.x/requests#retrieving-input

//     // boolean input values
//     // https://laravel.com/docs/8.x/requests#retrieving-a-portion-of-the-input-data
//     // $archived = request()->boolean('archived');
//     // $input = request()->only(['username', 'password']);
//     // $input = request()->only('username', 'password');

//     // $input = request()->except(['username', 'password']);
//     // $input = request()->except('username', 'password');

//     // dd((int)request()->query('page', 1));
//     return view('posts.index', ['posts' => $posts]);
// });

// Route::get('/posts/{id}', function ($id) use($posts) {
//     abort_if(!isset($posts[$id]), 404);
//     return view('posts.show', ['post' => $posts[$id]]);
// })
//     // ->where([ 
//     //     'id' => '[0-9]+'
//     // ]
//     // )
//     ->name('posts.show');

Route::resource('posts.comments', PostCommentController::class)->only(['store']);

Route::get('/recent-posts/{days_ago?}', function ($daysAgo = 20) {
    return 'Post from ' . $daysAgo . ' days ago';
})->name('posts.recent.index')->middleware('auth'); // ** auth is middleware route config in app/Http/Kernel.php


// Route::prefix('/fun')->name('fun.')->group(function() use($posts) {
//     Route::get('responses', function() use($posts ) {
//         return response($posts, 201)
//         ->header('Content-Type', 'application/json')
//         ->cookie('MY_COOKIE', 'Wiroj Khuntong', 3600);
//     })->name('responses');
    
//     Route::get('redirect', function(){
//         return redirect('/contact');
//     })->name('redirect');;
    
//     Route::get('back', function(){
//         return back();
//     })->name('back');;
    
//     Route::get('named-route', function(){
//         return redirect()->route('posts.show', ['id' => 1]);
//     })->name('named-route');;
    
//     Route::get('away', function(){
//         return redirect()->away('https://google.com');
//     })->name('away');;
    
//     Route::get('json', function() use($posts) {
//         return response()->json($posts);
//     })->name('json');;
    
//     Route::get('download', function(){
//         return response()->download(public_path('/daniel.jpg'));
//     })->name('download');;
// });

Route::resource('users', 'App\Http\Controllers\UserController')->only(['show', 'edit', 'update']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
