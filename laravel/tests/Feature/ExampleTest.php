<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\User;

use Illuminate\Support\Facades\Hash;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $user = User::create([
            'name' => 'xxx',
            'email' => 'xxx@gmail.com',
            'password' => Hash::make('isylzjko'),
         ]);

        $response = $this->actingAs($user);
        $response = $this->get('/');


        $response->assertStatus(200);
    }
}
