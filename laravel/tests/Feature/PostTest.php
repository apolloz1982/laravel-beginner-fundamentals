<?php

namespace Tests\Feature;

use App\Models\BlogPost;
use App\Models\Comment;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use App\Models\User;

use Illuminate\Support\Facades\Hash;

class PostTest extends TestCase
{
    use RefreshDatabase;
    // use WithoutMiddleware; // without csrf;
   

    public function testNoBlogPostsWhenNothingInDatabase()
    {
        $response = $this->get('/posts');

        $response->assertSeeText('No post found!');
    }

    public function testSee1BlogPostWhenThereIs1WithNoComments()
    {
        // Arrange
        $post = $this->createDummyBlogPost();

        // Act
        $response = $this->get('/posts');

        //Assert
        $response->assertSeeText('New title');
        $response->assertSeeText('No comments yet!');

        $this->assertDatabaseHas('blog_posts', [
            'title' => 'New title'
        ]);
    }

    public function testSee1BlogPostWithComments()
    {
        // Arrange
        $post = $this->createDummyBlogPost();

        Comment::factory()->count(4)->create([
            'blog_post_id' => $post->id
        ]);
        // Act
        $response = $this->get('/posts');

        //Assert
        $response->assertSeeText('4 comments');

    }

    public function testStoreValid() 
    {
        $user = $this->user();

        $params = [
            'title' => 'Valid title',
            'content' => 'At least 10 characters'
        ];


        $response = $this->actingAs($user)->post('/posts', $params);
        // $response->assertRedirect('posts/{post}');
        
        // Can't get session 'status' , because status put in flash message session  
        // $response->assertSessionHas('status');
        // $this->assertEquals(session('status'), 'The blog post was created!');

        // $response->assertStatus(302);
        // Change strategy check assert with
        $this->followRedirects($response)->assertSee('The blog post was created!');
        
    }

    public function testStoreFail()
    {
        $user = $this->user();

        $params = [
            'title' => 'x',
            'content' => 'x'
        ];

        $response = $this->actingAs($user)->post('/posts', $params);
        // $response->ddSession(); // debug session
        $response->assertStatus(302);
        $response->assertSessionHas('errors');
    }

    public function testUpdateValid()
    {
         // Arrange
         $user = $this->user();

         $post = $this->createDummyBlogPost($user->id);

         $this->assertDatabaseHas('blog_posts', $post->toArray());

         $params = [
            'title' => 'A new named titile',
            'content' => 'Content was changed'
        ];


        $response = $this->actingAs($user)->put("/posts/{$post->id}", $params);
        // $response->assertStatus(302);
        // $response->assertSessionHas('errors');
        $this->followRedirects($response)->assertSee('Blog post was updated!');

        $this->assertDatabaseMissing('blog_posts', $post->toArray());

        $this->assertDatabaseHas('blog_posts', [
            'title' => 'A new named titile',
        ]);
    }

    public function testDelete()
    {
        $user = $this->user();
        
        $post = $this->createDummyBlogPost();
        $this->assertDatabaseHas('blog_posts', $post->toArray());

        $response = $this->actingAs($user)->delete("/posts/{$post->id}");
        $this->followRedirects($response)->assertSee('Blog post was deleted!');

        // $this->assertDatabaseMissing('blog_posts', $post->toArray());
        $this->assertSoftDeleted('blog_posts', $post->toArray());
    }

    private function createDummyBlogPost($userId = null): BlogPost
    {
        // $post = new BlogPost();
        // $post->title = 'New title';
        // $post->content = 'Content of the blog post';
        // $post->save();

        return BlogPost::factory()
        ->newTitle()
        ->create([
            'user_id' => $userId ?? $this->user()->id
        ]);

        // return $post;
    }
}
