<?php

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    // [
    //     'name' => $this->faker->name,
    //     'email' => $this->faker->unique()->safeEmail,
    //     'email_verified_at' => now(),
    //     'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
    //     'remember_token' => Str::random(10),
    //     'is_admin' => false
    // ]

    protected function user() : User
    {
        return User::factory()->create([ 'is_admin' => true ]);
        // return  User::create([
        //     'name' => 'apolloz',
        //     'email' => 'apolloz1982@gmail.com',
        //     'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password,
        //     'remember_token' => Str::random(10),
        //     'is_admin' => false
        //  ]);
    }
}
