/**
*
* Introducing Tinker
*/

php artisan tinker
>>> $post = new BlogPost();
[!] Aliasing 'BlogPost' to 'App\Models\BlogPost' for this Tinker session.
=> App\Models\BlogPost {#4439}
>>> $post->title = 'I am the title';
=> "I am the title"
>>> $post->content = 'I am the content';
=> "I am the content"
>>> $post
=> App\Models\BlogPost {#4439
     title: "I am the title",
     content: "I am the content",
   }
>>> $post->save();
=> true

>>> $post->title = 'I ame changed!';
=> "I ame changed!"
>>> $post->save();
=> true

>>> BlogPost::find(1);
=> App\Models\BlogPost {#4451
     id: 1,
     created_at: "2022-01-22 07:39:55",
     updated_at: "2022-01-22 07:42:09",
     title: "I ame changed!",
     content: "I am the content",
   }


>>> BlogPost::findOrFail(1);
=> App\Models\BlogPost {#4452
     id: 1,
     created_at: "2022-01-22 07:39:55",
     updated_at: "2022-01-22 07:42:09",
     title: "I ame changed!",
     content: "I am the content",
   }
>>> BlogPost::findOrFail(10);
Illuminate\Database\Eloquent\ModelNotFoundException with message 'No query results for model [App\Models\BlogPost] 10'



>>> $posts = BlogPost::all();
=> Illuminate\Database\Eloquent\Collection {#4459
     all: [
       App\Models\BlogPost {#4460
         id: 1,
         created_at: "2022-01-22 07:39:55",
         updated_at: "2022-01-22 07:42:09",
         title: "I ame changed!",
         content: "I am the content",
       },
     ],
   }
>>> $posts[0]
=> App\Models\BlogPost {#4460
     id: 1,
     created_at: "2022-01-22 07:39:55",
     updated_at: "2022-01-22 07:42:09",
     title: "I ame changed!",
     content: "I am the content",
   }

>>> $posts->first();
=> App\Models\BlogPost {#4460
     id: 1,
     created_at: "2022-01-22 07:39:55",
     updated_at: "2022-01-22 07:42:09",
     title: "I ame changed!",
     content: "I am the content",
   }

>>> $posts->count();
=> 1

>>> BlogPost::find([1, 2, 3]);
=> Illuminate\Database\Eloquent\Collection {#4451
     all: [
       App\Models\BlogPost {#4456
         id: 1,
         created_at: "2022-01-22 07:39:55",
         updated_at: "2022-01-22 07:42:09",
         title: "I ame changed!",
         content: "I am the content",
       },
     ],
   }


https://laravel.com/docs/8.x/eloquent-collections#available-methods


/**
*   Query Builder
*/

>>> User::factory()->count(5)->create();
[!] Aliasing 'User' to 'App\Models\User' for this Tinker session.
<warning>PHP Deprecated:  Since fakerphp/faker 1.14: Accessing property "name" is deprecated, use "name()" instead. in /Users/wirojkhuntong/my_workspace/udemy/laravel-beginner-fundamentals/laravel/vendor/symfony/deprecation-contracts/function.php on line 25</warning>
<warning>PHP Deprecated:  Since fakerphp/faker 1.14: Accessing property "safeEmail" is deprecated, use "safeEmail()" instead. in /Users/wirojkhuntong/my_workspace/udemy/laravel-beginner-fundamentals/laravel/vendor/symfony/deprecation-contracts/function.php on line 25</warning>
<warning>PHP Deprecated:  Since fakerphp/faker 1.14: Accessing property "name" is deprecated, use "name()" instead. in /Users/wirojkhuntong/my_workspace/udemy/laravel-beginner-fundamentals/laravel/vendor/symfony/deprecation-contracts/function.php on line 25</warning>
<warning>PHP Deprecated:  Since fakerphp/faker 1.14: Accessing property "safeEmail" is deprecated, use "safeEmail()" instead. in /Users/wirojkhuntong/my_workspace/udemy/laravel-beginner-fundamentals/laravel/vendor/symfony/deprecation-contracts/function.php on line 25</warning>
<warning>PHP Deprecated:  Since fakerphp/faker 1.14: Accessing property "name" is deprecated, use "name()" instead. in /Users/wirojkhuntong/my_workspace/udemy/laravel-beginner-fundamentals/laravel/vendor/symfony/deprecation-contracts/function.php on line 25</warning>
<warning>PHP Deprecated:  Since fakerphp/faker 1.14: Accessing property "safeEmail" is deprecated, use "safeEmail()" instead. in /Users/wirojkhuntong/my_workspace/udemy/laravel-beginner-fundamentals/laravel/vendor/symfony/deprecation-contracts/function.php on line 25</warning>
<warning>PHP Deprecated:  Since fakerphp/faker 1.14: Accessing property "name" is deprecated, use "name()" instead. in /Users/wirojkhuntong/my_workspace/udemy/laravel-beginner-fundamentals/laravel/vendor/symfony/deprecation-contracts/function.php on line 25</warning>
<warning>PHP Deprecated:  Since fakerphp/faker 1.14: Accessing property "safeEmail" is deprecated, use "safeEmail()" instead. in /Users/wirojkhuntong/my_workspace/udemy/laravel-beginner-fundamentals/laravel/vendor/symfony/deprecation-contracts/function.php on line 25</warning>
<warning>PHP Deprecated:  Since fakerphp/faker 1.14: Accessing property "name" is deprecated, use "name()" instead. in /Users/wirojkhuntong/my_workspace/udemy/laravel-beginner-fundamentals/laravel/vendor/symfony/deprecation-contracts/function.php on line 25</warning>
<warning>PHP Deprecated:  Since fakerphp/faker 1.14: Accessing property "safeEmail" is deprecated, use "safeEmail()" instead. in /Users/wirojkhuntong/my_workspace/udemy/laravel-beginner-fundamentals/laravel/vendor/symfony/deprecation-contracts/function.php on line 25</warning>
=> Illuminate\Database\Eloquent\Collection {#4486
     all: [
       App\Models\User {#4476
         name: "Terrance Sawayn",
         email: "kariane28@example.org",
         email_verified_at: "2022-01-22 08:06:39",
         #password: "$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi",
         #remember_token: "P4eF9Ya0QF",
         updated_at: "2022-01-22 08:06:39",
         created_at: "2022-01-22 08:06:39",
         id: 1,
       },
       App\Models\User {#4472
         name: "Macie Mayer",
         email: "alaina.stamm@example.org",
         email_verified_at: "2022-01-22 08:06:39",
         #password: "$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi",
         #remember_token: "MSxdYK24ZS",
         updated_at: "2022-01-22 08:06:39",
         created_at: "2022-01-22 08:06:39",
         id: 2,
       },
       App\Models\User {#4474
         name: "Jordy Williamson",
         email: "hickle.francesco@example.com",
         email_verified_at: "2022-01-22 08:06:39",
         #password: "$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi",
         #remember_token: "ocNAxU8xuX",
         updated_at: "2022-01-22 08:06:39",
         created_at: "2022-01-22 08:06:39",
         id: 3,
       },
       App\Models\User {#4483
         name: "Brooke Hoeger II",
         email: "rwalker@example.org",
         email_verified_at: "2022-01-22 08:06:39",
         #password: "$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi",
         #remember_token: "NvpgrWVBHa",
         updated_at: "2022-01-22 08:06:39",
         created_at: "2022-01-22 08:06:39",
         id: 4,
       },
       App\Models\User {#4484
         name: "Mrs. Shirley Bernier",
         email: "vidal03@example.net",
         email_verified_at: "2022-01-22 08:06:39",
         #password: "$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi",
         #remember_token: "r6YbhHTpuh",
         updated_at: "2022-01-22 08:06:39",
         created_at: "2022-01-22 08:06:39",
         id: 5,
       },
     ],
   }




>>> User::where('id', '>=', 2)->orderBy('id', 'desc')->get()
=> Illuminate\Database\Eloquent\Collection {#4465
     all: [
       App\Models\User {#4464
         id: 5,
         name: "Mrs. Shirley Bernier",
         email: "vidal03@example.net",
         email_verified_at: "2022-01-22 08:06:39",
         #password: "$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi",
         #remember_token: "r6YbhHTpuh",
         created_at: "2022-01-22 08:06:39",
         updated_at: "2022-01-22 08:06:39",
       },
       App\Models\User {#4444
         id: 4,
         name: "Brooke Hoeger II",
         email: "rwalker@example.org",
         email_verified_at: "2022-01-22 08:06:39",
         #password: "$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi",
         #remember_token: "NvpgrWVBHa",
         created_at: "2022-01-22 08:06:39",
         updated_at: "2022-01-22 08:06:39",
       },
       App\Models\User {#4442
         id: 3,
         name: "Jordy Williamson",
         email: "hickle.francesco@example.com",
         email_verified_at: "2022-01-22 08:06:39",
         #password: "$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi",
         #remember_token: "ocNAxU8xuX",
         created_at: "2022-01-22 08:06:39",
         updated_at: "2022-01-22 08:06:39",
       },
       App\Models\User {#4485
         id: 2,
         name: "Macie Mayer",
         email: "alaina.stamm@example.org",
         email_verified_at: "2022-01-22 08:06:39",
         #password: "$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi",
         #remember_token: "MSxdYK24ZS",
         created_at: "2022-01-22 08:06:39",
         updated_at: "2022-01-22 08:06:39",
       },
     ],
   }

